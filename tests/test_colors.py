import unittest
import sys

from echocolor.colorizer import Colorizer
from echocolor.colors import blue, cyan, green, red, yellow, cyan_on_red


class TestTermColors(unittest.TestCase):
    """Some spot-check/regression tests of the color functions that are
    defined dynamically in the echocolor.colors module"""

    def setUp(self):
        Colorizer.force_color = True

    def tearDown(self):
        Colorizer.force_color = False

    def test_blue(self):
        msg = blue('test message')
        self.assertEqual(msg, '\x1b[38;5;4mtest message\x1b[0m')

    def test_cyan(self):
        msg = cyan('test message')
        self.assertEqual(msg, '\x1b[38;5;6mtest message\x1b[0m')

    def test_green(self):
        msg = green('test message')
        self.assertEqual(msg, '\x1b[38;5;2mtest message\x1b[0m')

    def test_red(self):
        msg = red('test message')
        self.assertEqual(msg, '\x1b[38;5;1mtest message\x1b[0m')

    def test_yellow(self):
        msg = yellow('test message')
        self.assertEqual(msg, '\x1b[38;5;3mtest message\x1b[0m')

    def test_cyan_on_red(self):
        msg = cyan_on_red('test message')
        self.assertEqual(msg, '\x1b[38;5;6m\x1b[48;5;1mtest message\x1b[0m')


if __name__ == '__main__':
    unittest.main()
