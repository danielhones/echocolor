import unittest
from io import StringIO

from echocolor.colorizer import Colorizer


class TestColorizer(unittest.TestCase):
    def test_uncolored(self):
        output = StringIO()
        colorizer = Colorizer('red', out=output)
        self.assertEqual(colorizer('test message'), 'test message')

    def test_fg_color(self):
        colorizer = Colorizer('red')
        msg = colorizer('test message', force=True)
        self.assertEqual(msg, '\x1b[38;5;1mtest message\x1b[0m')

    def test_bg_color(self):
        colorizer = Colorizer('red', 'blue')
        msg = colorizer('test message', force=True)
        self.assertEqual(msg, '\x1b[38;5;1m\x1b[48;5;4mtest message\x1b[0m')

    def test_name(self):
        colorizer = Colorizer('cyan')
        self.assertEqual(colorizer.__name__, 'cyan')

    def test_name_with_bg_color(self):
        colorizer = Colorizer('cyan', 'yellow')
        self.assertEqual(colorizer.__name__, 'cyan_on_yellow')


if __name__ == '__main__':
    unittest.main()
