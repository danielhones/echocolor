import unittest
from echocolor.escape_codes import COLOR_CODES, fg, bg, style


class TestEscapeCodes(unittest.TestCase):
    """Spot check a few of the escape codes"""

    def test_red_code(self):
        self.assertEqual(COLOR_CODES['red'], 1)

    def test_green_code(self):
        self.assertEqual(COLOR_CODES['green'], 2)

    def test_white_code(self):
        self.assertEqual(COLOR_CODES['white'], 15)

    def test_fg_cyan(self):
        self.assertEqual(fg('cyan'), '\x1b[38;5;6m')

    def test_fg_magenta(self):
        self.assertEqual(fg('magenta'), '\x1b[38;5;5m')

    def test_fg_light_blue(self):
        self.assertEqual(fg('light_blue'), '\x1b[38;5;12m')

    def test_bg_light_green(self):
        self.assertEqual(bg('light_green'), '\x1b[48;5;10m')

    def test_bg_yellow(self):
        self.assertEqual(bg('yellow'), '\x1b[48;5;3m')

    def test_bg_light_magenta(self):
        self.assertEqual(bg('light_magenta'), '\x1b[48;5;13m')

    def test_style_reset(self):
        self.assertEqual(style('reset'), '\x1b[0m')

    def test_style_bold(self):
        self.assertEqual(style('bold'), '\x1b[1m')

    def test_style_dim(self):
        self.assertEqual(style('dim'), '\x1b[2m')

    def test_style_italic(self):
        self.assertEqual(style('italic'), '\x1b[3m')

    def test_style_underlined(self):
        self.assertEqual(style('underlined'), '\x1b[4m')


if __name__ == "__main__":
    unittest.main()
