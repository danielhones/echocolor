import unittest
import os
import sys

if sys.version_info[0] < 3:
    from io import BytesIO as StringIO
else:
    from io import StringIO

from echocolor.terminal_echo import TerminalEcho, echo, err


class TestTerminalEcho(unittest.TestCase):
    def setUp(self):
        self.output = StringIO()
        self.test_echo = TerminalEcho(self.output)

    def _read_output(self):
        self.output.seek(0)
        return self.output.read()

    def assertOutput(self, message):
        self.assertEqual(self._read_output(), message)

    def test_tty_out_false(self):
        self.assertFalse(self.test_echo._tty_out)

    @unittest.skipUnless(sys.stdout.isatty(), 'STDOUT is not a terminal, test would fail')
    def test_echo_tty_out(self):
        self.assertTrue(echo._tty_out)

    @unittest.skipUnless(sys.stderr.isatty(), 'STDERR is not a terminal, test would fail')
    def test_err_tty_out(self):
        self.assertTrue(err._tty_out)

    def test_format_includes_newline_by_default(self):
        msg = self.test_echo._format('test message')
        self.assertEqual(msg, 'test message\n')

    def test_format_omits_newline(self):
        msg = self.test_echo._format('test message', newline=False)
        self.assertEqual(msg, 'test message')

    def test_call_prints_uncolored_text(self):
        self.test_echo.force_color = True
        self.test_echo('test message')
        self.assertOutput('test message\n')

    def test_echo_is_uncolored_when_not_a_tty(self):
        self.test_echo.cyan('test message')
        self.assertOutput('test message\n')

    def test_colored_echo(self):
        self.test_echo.force_color = True
        self.test_echo.cyan('test message')
        self.assertOutput('\x1b[38;5;6mtest message\x1b[0m\n')

    def test_colored_echo_omit_newline(self):
        self.test_echo.force_color = True
        self.test_echo.cyan('test message', newline=False)
        self.assertOutput('\x1b[38;5;6mtest message\x1b[0m')

    def test_colored_echo_force_kwarg(self):
        self.test_echo.cyan('test message', force=True)
        self.assertOutput('\x1b[38;5;6mtest message\x1b[0m\n')

    def test_bold_is_normal_when_not_tty(self):
        self.test_echo.bold('test message')
        self.assertOutput('test message\n')

    def test_bold_echo(self):
        self.test_echo.force_color = True
        self.test_echo.bold('test message')
        self.assertOutput('\x1b[1mtest message\x1b[0m\n')

    def test_bold_echo_force_kwarg(self):
        self.test_echo.bold('test message', force=True)
        self.assertOutput('\x1b[1mtest message\x1b[0m\n')

    def test_underline_is_normal_when_not_tty(self):
        self.test_echo.underline('test message')
        self.assertOutput('test message\n')

    def test_underline_echo(self):
        self.test_echo.force_color = True
        self.test_echo.underline('test message')
        self.assertOutput('\x1b[4mtest message\x1b[0m\n')

    def test_underline_force_kwarg(self):
        self.test_echo.underline('test message', force=True)
        self.assertOutput('\x1b[4mtest message\x1b[0m\n')


if __name__ == '__main__':
    unittest.main()
