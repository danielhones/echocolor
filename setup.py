from setuptools import setup


VERSION = '2.0.0'
package_name = 'echocolor'
extras_require = {}
packages = [
    package_name,
]
classifiers = [
    'Programming Language :: Python :: 2.7',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6',
]
package_data = {
    '': ['LICENSE', 'README.rst']
}
with open('README.rst') as f:
    long_description = f.read()


setup(
    name=package_name,
    packages=packages,
    version=VERSION,
    author="Daniel Hones",
    url="https://gitlab.com/danielhones/echocolor",
    description="Simple interface for printing colored text to a terminal",
    keywords=["terminal", "ansi", "print", "color"],
    classifiers=classifiers,
    license='MIT',
    extras_require=extras_require,
    package_data=package_data,
    long_description=long_description,
)
